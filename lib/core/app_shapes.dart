import 'package:flutter/material.dart';

class AppShapes {
  static MaterialStateProperty<OutlinedBorder> materialShapeColor(
      Color colorSide, double circle) {
    return MaterialStateProperty.all<OutlinedBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(circle),
        ),
        side: BorderSide(color: colorSide, width: 2),
      ),
    );
  }

  static OutlinedBorder shape(double circle) {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(circle),
      ),
    );
  }

  static OutlinedBorder shapeColor(double circle, color) {
    return RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(circle),
      ),
      side: BorderSide(color: color, width: 2),
    );
  }
}
