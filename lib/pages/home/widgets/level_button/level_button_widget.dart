import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz/core/core.dart';
import 'package:quiz/pages/home/widgets/level_button/level_button_config.dart';

class LevelButtonWidget extends StatelessWidget {
  final String textButton;

  const LevelButtonWidget({
    Key? key,
    required this.textButton,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    LevelButtonConfig levelButtonConfig = LevelButtonConfig(level: textButton);
    print(levelButtonConfig.fontColor);
    return Container(
      child: OutlinedButton(
        style: ButtonStyle(
          elevation: MaterialStateProperty.all<double>(0),
          shape:
              AppShapes.materialShapeColor(levelButtonConfig.borderColor, 28),
          backgroundColor:
              AppColors.convertForMaterialColor(levelButtonConfig.color),
        ),
        onPressed: () {},
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 6),
          child: Text(
            textButton,
            style: GoogleFonts.notoSans(
                color: levelButtonConfig.fontColor, fontSize: 13),
          ),
        ),
      ),
    );
  }
}
