import 'package:flutter/material.dart';
import 'package:quiz/core/core.dart';

class Level {
  static String facil = 'Fácil';
  static String medio = 'Médio';
  static String dificil = 'Difícil';
  static String perito = 'Perito';
}

class LevelButtonConfig {
  final String level;
  LevelButtonConfig({required this.level});

  final config = {
    Level.facil: {
      'color': AppColors.levelButtonFacil,
      'borderColor': AppColors.levelButtonBorderFacil,
      'fontColor': AppColors.levelButtonTextFacil,
    },
    Level.medio: {
      'color': AppColors.levelButtonMedio,
      'borderColor': AppColors.levelButtonBorderMedio,
      'fontColor': AppColors.levelButtonTextMedio,
    },
    Level.dificil: {
      'color': AppColors.levelButtonDificil,
      'borderColor': AppColors.levelButtonBorderDificil,
      'fontColor': AppColors.levelButtonTextDificil,
    },
    Level.perito: {
      'color': AppColors.levelButtonPerito,
      'borderColor': AppColors.levelButtonBorderPerito,
      'fontColor': AppColors.levelButtonTextPerito,
    },
  };

  Color get color => config[level]!['color']!;
  Color get borderColor => config[level]!['borderColor']!;
  Color get fontColor => config[level]!['fontColor']!;
}
