import 'package:flutter/material.dart';
import 'package:quiz/core/core.dart';
import 'package:quiz/widgets/linear_progress_widget.dart';

class QuizCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.white,
      shape: AppShapes.shape(10),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 40,
              width: 40,
              child: Image.asset(AppImages.blocks),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Gerenciamento de estado',
              style: AppTextStyles.heading15,
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    '3 de 10',
                    style: AppTextStyles.body11,
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: LinearProgressWidget(
                    value: 0.3,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
