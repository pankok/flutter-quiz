import 'package:flutter/material.dart';
import 'package:quiz/pages/home/widgets/level_button/level_button_config.dart';
import 'package:quiz/widgets/app_bar_widget.dart';

import 'widgets/level_button/level_button_widget.dart';
import 'widgets/quiz_card/quiz_card_widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Column(
        children: [
          SizedBox(
            height: 24,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              LevelButtonWidget(
                textButton: Level.facil,
              ),
              LevelButtonWidget(
                textButton: Level.medio,
              ),
              LevelButtonWidget(
                textButton: Level.dificil,
              ),
              LevelButtonWidget(
                textButton: Level.perito,
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
          Expanded(
              child: GridView.count(
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 5,
            children: [
              QuizCardWidget(),
              QuizCardWidget(),
              QuizCardWidget(),
              QuizCardWidget(),
            ],
          ))
        ],
      ),
    );
  }
}
