import 'package:flutter/material.dart';
import 'package:quiz/core/app_text_styles.dart';
import '../awnser/awnser_widget.dart';

class QuizWidget extends StatelessWidget {
  final String title;

  const QuizWidget({Key? key, required this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            title,
            style: AppTextStyles.heading,
          ),
          SizedBox(height: 20),
          AwnserWidget(
            title: 'Possibilidade de ser compilado nativamente',
            isRight: true,
            isSelected: true,
          )
        ],
      ),
    );
  }
}
