import 'package:flutter/material.dart';
import 'package:quiz/core/core.dart';
import 'package:quiz/widgets/linear_progress_widget.dart';

class QuestIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Questão 04',
                style: AppTextStyles.body,
              ),
              Text(
                'de 10',
                style: AppTextStyles.body,
              ),
            ],
          ),
          SizedBox(height: 15),
          LinearProgressWidget(
            value: 0.4,
          ),
        ],
      ),
    );
  }
}
