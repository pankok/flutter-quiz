import 'package:flutter/material.dart';
import 'package:quiz/pages/challenge/widgets/quest_indicator/quest_indicator_widget.dart';

import 'widgets/quiz/quiz_widget.dart';

class ChallengePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: SafeArea(
          child: QuestIndicatorWidget(),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 40),
        child: QuizWidget(
          title: 'Ola marile',
        ),
      ),
    );
  }
}
