import 'package:flutter/material.dart';
import 'package:quiz/pages/home/home_page.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "quiz",
      home: HomePage(),
    );
  }
}
